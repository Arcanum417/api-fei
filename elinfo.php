<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="styly.css">
  <link rel="icon" type="image/png" href="pics/favicon.png">
  <title>FEI API od 2015</title>
  </head>
  
  <body>
  <table width="1200" align="center" border="0" cellpadding="10" cellspacing="0">
   <tr>
    <td width="200" valign="top">
    
    <div class="change" style="position: absolute;top: 20px;">
    <a style="position:fixed;" href="http://www.fei.stuba.sk/"><img class="change" src="pics/fei_logo.png" border="0" height="47" width="140"></a>
    </div>
    
    <div class="text_in_menu">
		<?php include 'menu.php';?>
    </div>
    </td>
    
    <td width="800" valign="top">
    
    <div class="text_in_heading">      
		<?php include 'heading.php';?>
    </div>
    
    <div class="text_in_body">
    
    <span class="mytable">
    <table width="0" cellspacing="0" cellpadding="0" border="1">
       <tr style="border-bottom:1px; border-bottom-color:rgb(240,185,56)">
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px" width="120">Dátum prednášky</td>
         <td style="padding-top:4px" width="40"><span class="change"><a href="files/elinfo/poznamky/"><img src="pics/notes_logo.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">17.2.2016</td>
         <td><span class="change"><a href="files/elinfo/poznamky/pelinfo_17.2.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">24.2.2016</td>
         <td><span class="change"><a href="files/elinfo/poznamky/pelinfo_24.2.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">2.3.2016</td>
         <td><span class="change"><a href="files/elinfo/poznamky/pelinfo_2.3.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">9.3.2016</td>
         <td><span class="change"><a href="files/elinfo/poznamky/pelinfo_9.3.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">16.3.2016</td>
         <td><span class="change"><a href="files/elinfo/poznamky/pelinfo_16.3.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">23.3.2016</td>
         <td><img style="padding-top:4px" src="pics/holiday_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">30.3.2016</td>
         <td><span class="change"><a href="files/elinfo/poznamky/pelinfo_30.3.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">6.4.2016</td>
         <td><span class="change"><a href="files/elinfo/poznamky/pelinfo_6.4.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">13.4.2016</td>
         <td><span class="change"><a href="files/elinfo/poznamky/pelinfo_13.4.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">20.4.2016</td>
         <td><span class="change"><a href="files/elinfo/poznamky/pelinfo_20.4.2016.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">27.4.2016</td>
         <td><span class="change"><a href="files/elinfo/poznamky/pelinfo_27.4.2016.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">4.5.2016</td>
         <td><span class="change"><a href="files/elinfo/poznamky/pelinfo_4.5.2016.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>                                                                                     
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">11.5.2016</td>
         <td><img style="padding-top:4px" src="pics/red_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
    </table>
    </span><br>   
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Materiály<br>
    <div style="padding-left:21px;width:500px">
    - <a href="files/elinfo/prezentacie.zip">Prednášky z minulých rokov (nie aktuálne, ale užitočné)</a><br>
    - <a href="files/elinfo/elinfo_vzorove_priklady.zip">Vzorové príklady</a><br>
    - <a href="files/elinfo/elinfo_teoria.zip">Teória (3 rôzne typy materiálov)</a><br>
    - <a href="files/elinfo/elinfo_teoria.pdf">Teória z poznámok z minulého roku</a><br>
    - <a href="files/elinfo/elinfo_teoria_v2.pdf">Ešte viacej teórie (nejaká česká, ale k veci)</a><br>    
    - <a href="files/elinfo/elinfo_teoria_soka.docx">Aktuálna teória z tohoto roku</a><br>
    </div><br> 
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Labáky<br>
    <div style="padding-left:21px;width:500px">
    - <a href="files/elinfo/prazdne_labaky.zip">Prázdne labáky</a>
    </div><br>
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Skúška & zápočty<br>
    <div style="padding-left:21px">
    - <a href="files/elinfo/skuska_elinfo.zip">Tohtoročná skúška z elinfo</a><br>
    </div><br>
                                                                                                 
    </div>
    </td>
    
    <td width="300" valign="top">
    <div class="text_in_side"><br>
		<?php include 'news.php';?>
    </div>
    </td>
    
   </tr>
  </table>
  
  </body>
</html>   