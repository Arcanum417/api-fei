<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="styly.css">
  <link rel="icon" type="image/png" href="pics/favicon.png">
  <title>FEI API od 2015</title>
  </head>
  
  <body>
  <table width="1200" align="center" border="0" cellpadding="10" cellspacing="0">
   <tr>
    <td width="200" valign="top">
    
    <div class="change" style="position: absolute;top: 20px;">
    <a style="position:fixed;" href="http://www.fei.stuba.sk/"><img class="change" src="pics/fei_logo.png" border="0" height="47" width="140"></a>
    </div>
    
    <div class="text_in_menu">
		<?php include 'menu.php';?>
    </div>
    </td>
    
    <td width="800" valign="top">
    
    <div class="text_in_heading">      
		<?php include 'heading.php';?>
    </div>
    
    <div class="text_in_body">
    
    <div>
    BASIC INFO
    <ul>
      <li>1 zápočtová písomka (30b)</li>
      <li>zápočet udelený pri aspoň 15b</li>
      <li>opravný zápočet sa koná (minimum pravdepodobne netreba)</li>
      <li>skúška sa koná (70b)</li>
      <li>kalkulačky na písomkách sú zakázané</li>
      <li>ťahák je povolený (list A5, nutné mať na ňom napísané: Meno Priezvisko, <br>ťahák na LA, odovzdáva sa spolu s písomkou)</li>
      <li>Web stránka predmetu ... <span class="change"><a target="_blank" href="http://matika.elf.stuba.sk/KMAT/LinAlgeLinearneProgramovanie"><img src="pics/web_icon.png" border="0" height="16" width="16"></a></span></li>
    </ul>
    </div><br>
    
    <span class="mytable">
    <table width="0" cellspacing="0" cellpadding="0" border="1">
       <tr style="border-bottom:1px; border-bottom-color:rgb(240,185,56)">
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px" width="120">Dátum prednášky</td>
         <td style="padding-top:4px" width="40"><span class="change"><a href="files/la/prednasky/"><img src="pics/notes_logo.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">19.9.2016</td>
         <td><span class="change"><a href="files/la/prednasky/pla_19.9.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">26.9.2016</td>
         <td><span class="change"><a href="files/la/prednasky/pla_26.9.2016.pdf"><img style="padding-top:4px" src="pics/download_icon2.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">3.10.2016</td>
         <td><img style="padding-top:4px" src="pics/yellow_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">10.10.2016</td>
         <td><img style="padding-top:4px" src="pics/yellow_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">17.10.2016</td>
         <td><img style="padding-top:4px" src="pics/yellow_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">24.10.2016</td>
         <td><img style="padding-top:4px" src="pics/yellow_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">31.10.2016</td>
         <td><img style="padding-top:4px" src="pics/holiday_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">7.11.2016</td>
         <td><img style="padding-top:4px" src="pics/yellow_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">14.11.2016</td>
         <td><img style="padding-top:4px" src="pics/yellow_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">21.11.2016</td>
         <td><img style="padding-top:4px" src="pics/yellow_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">28.11.2016</td>
         <td><img style="padding-top:4px" src="pics/red_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">5.12.2016</td>
         <td><img style="padding-top:4px" src="pics/red_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">??.12.2016</td>
         <td><img style="padding-top:4px" src="pics/red_marker.png" border="0" height="16" width="16"></td>
       </tr>
       
    </table>
    </span><br><br>
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Materiály<br>
    <div style="padding-left:21px">
    - <a href="files/la/prednasky_od_zajaca_part_1.pdf">Prednášky od Zajaca part I.</a><br>
    </div><br>
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Skúška & zápočty<br>
    <div style="padding-left:21px">
    - <a href="files/la/vzory_skusok_la.zip">Skúšky z predošlých rokov</a><br>
    - <a href="files/la/zapoctovky_la.zip">Zápočtovky z predošlých rokov</a><br>
    - <a href="files/la/zapoctovka_la_1_2016.zip">1. zápočtovka (zadanie, 2016)</a><br>
    </div><br>
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Príklady<br>
    <div style="padding-left:21px">
    - <a href="files/la/prikladiky_la.zip">Príklady na precvičenie</a><br>
    - <a href="files/la/priklady/priklady1.pdf">Príklady 1 (z ÚIM)</a><br>
    - <a href="files/la/priklady/priklady2.pdf">Príklady 2 (z ÚIM)</a><br>
    - <a href="files/la/priklady/priklady3.pdf">Príklady 3 (z ÚIM)</a><br>
    - <a href="files/la/priklady/priklady4a5.pdf">Príklady 4 (z ÚIM)</a><br>
    - <a href="files/la/priklady/priklady6.pdf">Príklady 6 (z ÚIM)</a><br>
    - <a href="files/la/priklady/priklady7.pdf">Príklady 7 (z ÚIM)</a><br>
    - <a href="files/la/priklady/priklady8.pdf">Príklady 8 (z ÚIM)</a><br>
    <!-- - <a href="files/la/priklady/priklady9.pdf">Príklady 9 (z ÚIM)</a><br>
    - <a href="files/la/priklady/priklady10.pdf">Príklady 10 (z ÚIM)</a><br>
    - <a href="files/la/priklady/priklady11.pdf">Príklady 11 (z ÚIM)</a><br>
    - <a href="files/la/priklady/priklady12.pdf">Príklady 12 (z ÚIM)</a><br>-->
    </div><br><br><br>
    
    </div>
    </td>
    
    <td width="300" valign="top">
    <div class="text_in_side"><br>
		<?php include 'news.php';?>
    </div>
    </td>
    
   </tr>
  </table>
  
  </body>
</html>   