<!------------------------------------------------------------------------------------------------------------------------------------------------------------------->
<span class="date">20.4.17</span> <span class="novinka_text">NEW!</span>
Stránka je na novej adrese https://api-fei.tk a ma nového vlastníka. Úbohú stránku som zachránil s pazúrov Autora a zahostil. Uvidíme ako to bude s pridávaním obsahu. Každopádne hľadám dobrovoľníkov ;)<br><br>
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------->
<span class="date">15.11.16</span> <br>Konečne som doplnil veľkú väčšinu vecí, čo chýbali, ešte musím zohnať poznámky z 
<a href="la.html">LA</a> a <a href="m2o.html">M2 <i>(o)</i></a>.<br><br>
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------->
<span class="date">4.10.16</span> <br>Materiály sú ako tak podopĺňané, pri niektorých predmetoch si ešte nie som istý ako budem materiály poskytovať.
<br><br>Rád by som len upozornil, že som poskytol nejaký <a href="files/ps/zapocet_1_ps.zip">materiál</a> pre PS, pre 1.zápočtovku, je to zopár príkazov z minulého roka, určite odporúčam si ich prebehnúť.<br><br>
Do predmetu <a href="mpp.html">MPP</a> budem dávať aj vypracované zadania ak to bude možné. <font color="red">S ručením obmedzeným!</font><br><br>
Minuloročné predmety spojazdním asap.<br><br>
<!------------------------------------------------------------------------------------------------------------------------------------------------------------------->
<center><font color="white">--- 4. semester ---</font></center><br>