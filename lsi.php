<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="styly.css">
  <link rel="icon" type="image/png" href="pics/favicon.png">
  <title>FEI API od 2015</title>
  </head>
  
  <body>
  <table width="1200" align="center" border="0" cellpadding="10" cellspacing="0">
   <tr>
    <td width="200" valign="top">
    
    <div class="change" style="position: absolute;top: 20px;">
    <a style="position:fixed;" href="http://www.fei.stuba.sk/"><img class="change" src="pics/fei_logo.png" border="0" height="47" width="140"></a>
    </div>
    
    <div class="text_in_menu">
		<?php include 'menu.php';?>
    </div>
    </td>
    
    <td width="800" valign="top">
    
    <div class="text_in_heading">      
		<?php include 'heading.php';?> 
    </div>
    
    <div class="text_in_body">
    
    <span class="mytable">
    <table width="0" cellspacing="0" cellpadding="0" border="1">
       <tr style="border-bottom:1px; border-bottom-color:rgb(240,185,56)">
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px" width="120">Prednáška</td>
         <td style="padding-top:4px" width="40"><span class="change"><a href="files/lsi/nafotene_prednasky/"><img src="pics/presentation1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#1</td>
         <td><span class="change"><a href="files/lsi/nafotene_prednasky/ls1.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#2</td>
         <td><span class="change"><a href="files/lsi/nafotene_prednasky/ls2.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>  
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#3</td>
         <td><span class="change"><a href="files/lsi/nafotene_prednasky/ls3.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>  
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#4</td>
         <td><span class="change"><a href="files/lsi/nafotene_prednasky/ls4.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>  
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#5</td>
         <td><span class="change"><a href="files/lsi/nafotene_prednasky/ls5.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>  
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#6</td>
         <td><span class="change"><a href="files/lsi/nafotene_prednasky/ls6.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>  
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#7</td>
         <td><span class="change"><a href="files/lsi/nafotene_prednasky/ls7.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>  
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#8</td>
         <td><span class="change"><a href="files/lsi/nafotene_prednasky/ls8.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>  
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#9</td>
         <td><span class="change"><a href="files/lsi/nafotene_prednasky/ls9.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>  
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#10</td>
         <td><span class="change"><a href="files/lsi/nafotene_prednasky/ls10.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>  
       
       <tr>
         <td align="left" style="padding-top:2px;padding-bottom:2px;padding-left:3px">#11</td>
         <td><span class="change"><a href="files/lsi/nafotene_prednasky/ls11.zip"><img style="padding-top:4px" src="pics/download_icon1.png" border="0" height="16" width="16"></a></span></td>
       </tr>  
       
    </table><br>
    </span>
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Materiály<br>
    <div style="padding-left:21px">
    - <a href="files/lsi/priklady.zip">Príklady na prepočítanie (iba časť je aktuálna)</a><br>
    - <a href="files/lsi/ls_skripta.pdf">Skriptá LSI (odporúčam ale pozrieť prednášky)</a><br>
    </div><br>                                 
    
    <img src="arrow_o.png" border="0" height="12" width="12"> Skúška & zápočty<br>
    <div style="padding-left:21px">
    - <a href="files/lsi/vzory_z_minulych_skusok_lsi.zip">Skúšky z predošlých rokov</a><br>
    - <a href="files/lsi/tohtorocna_skuska.zip">Skúška z tohoto roku</a><br>
    </div><br>
    
    </div>
    </td>
    
    <td width="300" valign="top">
    <div class="text_in_side"><br>
		<?php include 'news.php';?>
    </div>
    </td>
    
   </tr>
  </table>
  
  </body>
</html>   